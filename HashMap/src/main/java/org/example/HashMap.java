package org.example;

import java.util.Arrays;

public class HashMap<K, V> {

    private Node<K, V>[] hashtable;

    private static final int capacity = 16;
    private double load_factor = 0.75;
    private int size = 0;

    public HashMap() {
        hashtable = (Node<K, V>[]) new Node<?, ?>[capacity];
    }

    public HashMap(int initialCapacity) {
        hashtable = (Node<K, V>[]) new Node<?, ?>[initialCapacity];
    }

    public HashMap(int initialCapacity, double load_factor_custom) {
        hashtable = (Node<K, V>[]) new Node<?, ?>[initialCapacity];
        this.load_factor = load_factor_custom;
    }


    /**
     * This function returns the size of the array
     *
     * @return the size of the array
     */
    public String size() {
        int size = 0;
        for (Node<K, V> o : hashtable) {
            if (o != null) {
                size++;
                Node<K, V> curr = o;
                while (curr.next != null) {
                    size++;
                    curr = curr.next;
                }
            }
        }
        return "Size is " + size + ". Capacity is " + hashtable.length;
    }

    /**
     * This function shows us if the array is empty or not
     *
     * @return if the array is empty or not
     */
    public boolean isEmpty() {
        int not_empty = 0;
        for (Node<K, V> n : hashtable) {
            if (n != null) {
                not_empty++;
                break;
            }

        }
        return not_empty != 1;
    }

    /**
     * This function cleans every key in the hashtable
     */
    public void clear() {
        for (int j = size; j > 0; j--) {
            --size;
        }
        hashtable = (Node<K, V>[]) new Node<?, ?>[capacity];
    }

    public void put(K key, V value) {
        if ((float) size / capacity == load_factor)
            hashtable = Arrays.copyOf(hashtable, hashtable.length * 2);

        Node<K, V> newNode = new Node<>(key, value, null);
        int hashIndex = key.hashCode() % capacity;

        System.out.println(hashIndex);


        Node<K, V> exists = hashtable[hashIndex];
        if (exists == null) {
            hashtable[hashIndex] = newNode;
            size++;
        } else {
            while (exists.next != null) {
                if (exists.key.equals(key)) {
                    exists.value = value;
                    return;
                }
            }
            if (exists.key.equals(key)) {
                exists.value = value;
            } else {
                exists.next = newNode;
                size++;
            }
        }
    }

    void remove(K key) {

        for(int i = 0;i<capacity;++i){
            Node<K,V> curr_node = hashtable[i];

            if(curr_node == null) continue;

            if (curr_node.key.equals(key)) {
                if (curr_node.next != null) {
                    Node<K,V> new_node = hashtable[i].next;
                    hashtable[i] = new_node;
                }
                else {hashtable[i] = null;
                }
                size--;

            } else {
                Node<K,V> current = hashtable[i];
                while (current.next != null) {
                    if (current.next.key.equals(key)) {
                        if (current.next.next == null) {
                            current.next = null;
                        } else {
                            current.next = current.next.next;
                        }
                        size--;
                        break;
                    }
                    current = current.next;
                }
            }


        }

//        for (Node<K, V> n : hashtable) {
//            Node<K, V> curr_node = n;
//
//            if (n == null) continue;
//
//            if (n.key.equals(key)) {
//                if (curr_node.next != null) n = n.next;
//                else n=null;
//
//            } else {
//                while (n.next != null) {
//                    if (n.next.key.equals(key)) {
//                        if (n.next.next == null) {
//                            n.next = null;
//                        } else {
//                            n.next = curr_node.next.next;
//                        }
//                        size--;
//                        break;
//                    }
//                    n = n.next;
//                }
//            }
//        }

    }

    void printHashMap() {
        for (Node<K, V> n : hashtable) {
            if (n == null) {
                continue;
            }
            System.out.println(n.toString());

        }
    }

}
